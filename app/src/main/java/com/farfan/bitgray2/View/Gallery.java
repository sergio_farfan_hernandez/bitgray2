package com.farfan.bitgray2.View;


import android.content.Context;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.farfan.bitgray2.Model.DBAdapter;
import com.farfan.bitgray2.R;
import com.squareup.picasso.Picasso;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Gallery extends Fragment {

    TextView tvAlbumTitle;
    DBAdapter db;
    ArrayList<String> arrListUrlPhotos = new ArrayList<>();
    GridView gridView;

    public Gallery() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        DrawerActivity drawerActivity = (DrawerActivity)getActivity();
        int mValue = drawerActivity.MyMethod();
        Log.e("Gallery", String.valueOf(mValue));

        View rootView = inflater.inflate(R.layout.fragment_gallery, container, false);

        tvAlbumTitle = (TextView) rootView.findViewById(R.id.albumTitle);
        gridView = (GridView) rootView.findViewById(R.id.gridview);

        //Get Album from SQLite
        db = new DBAdapter(getContext());

        try {
            db.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Cursor cStpValues = db.getAlbum();

        if(cStpValues.moveToFirst()){
            do{
                String strName = cStpValues.getString(1);

                //Show all values
                tvAlbumTitle.setText(strName);

            }while (cStpValues.moveToNext());

        }

        //Get Photos from SQLite

        Cursor curPhotos = db.getPhotos();
        if(curPhotos.moveToFirst()){
            do{

                String strUrl = curPhotos.getString(1);

                arrListUrlPhotos.add(strUrl);

            }while (curPhotos.moveToNext());

        }

        db.close();

        //Call Adapter
        gridView.setAdapter(new ImageAdapter(getActivity()));

        // Inflate the layout for this fragment
        return rootView;
    }


    //*************INNER CLASSES****************

    public class ImageAdapter extends BaseAdapter{

        private Context context;

        public ImageAdapter(Context c) {
            context = c;
        }

        @Override
        public int getCount() {
            Log.e("URL", String.valueOf(arrListUrlPhotos.size()));
            return arrListUrlPhotos.size();
        }

        @Override
        public String getItem(int i) {
            return arrListUrlPhotos.get(i);
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View convertView, ViewGroup viewGroup) {

            ImageView view = (ImageView) convertView;
            if (view == null) {
                view = new ImageView(getActivity());
                view.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }
            // Get the image URL for the current position.
            String url = getItem(i);

            Log.e("URL", arrListUrlPhotos.get(i));
            Picasso.with(context) //
                    .load(url)
                    .error(R.drawable.inicio_sesion)//
                    .into(view);


            return view;
        }
    }


}
