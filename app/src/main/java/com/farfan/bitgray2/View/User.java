package com.farfan.bitgray2.View;


import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.farfan.bitgray2.Model.DBAdapter;
import com.farfan.bitgray2.R;

import java.sql.SQLException;

/**
 * A simple {@link Fragment} subclass.
 */
public class User extends Fragment {

    TextView name, username, email, phone, company;
    DBAdapter db;

    public User() {

        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_user, container, false);

        name = (TextView)rootView.findViewById(R.id.textName);
        username = (TextView)rootView.findViewById(R.id.textUser);
        email = (TextView)rootView.findViewById(R.id.textEmail);
        phone = (TextView)rootView.findViewById(R.id.textPhone);
        company= (TextView)rootView.findViewById(R.id.textCompany);

        //Get data from SQLite
        db = new DBAdapter(getContext());

        try {
            db.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        Cursor cStpValues = db.getUSer();

        if(cStpValues.moveToFirst()){
            do{
                String strName = cStpValues.getString(1);
                String strUserName = cStpValues.getString(2);
                String strEmail = cStpValues.getString(3);
                String strPhone = cStpValues.getString(4);
                String strCompany = cStpValues.getString(5);
                //Show all values
                name.setText(strName);
                username.setText(strUserName);
                email.setText(strEmail);
                phone.setText(strPhone);
                company.setText(strCompany);

            }while (cStpValues.moveToNext());

        }

        db.close();


        return rootView;

    }


}
