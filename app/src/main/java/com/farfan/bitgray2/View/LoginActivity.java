package com.farfan.bitgray2.View;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.farfan.bitgray2.Controller.ListCommentAdapter;
import com.farfan.bitgray2.Model.DBAdapter;
import com.farfan.bitgray2.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {

    //************  GLOBAL ATTRIBUTES  ************
    private EditText username, password;
    private DBAdapter dbAdapter;
    static final int  BUFFER_SIZE = 1024;
    //
    final String WEBSERVICE_POSTS = "http://jsonplaceholder.typicode.com/posts";
    final String  WEB_SERVICE =  "http://jsonplaceholder.typicode.com/users";
    final String  WEB_SERVICE_ALBUM =  "http://jsonplaceholder.typicode.com/albums";
    final String  WEB_SERVICE_PHOTOS =  "http://jsonplaceholder.typicode.com/photos";
    final String WEBSERVICE_COMMENTS = "http://jsonplaceholder.typicode.com/comments";
    private int id_user = 20 , idJsonAlbum = 0 ;
    int postId = 0;

    //************  OVERRIDE METHODS  ************
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        username = (EditText) findViewById(R.id.editText1);
        password = (EditText) findViewById(R.id.editText2);

        copyDatabaseIntoPackage();

    }


    //************  PROPER METHODS  ************

    /*Validate special characters. Taken from:
    http://stackoverflow.com/questions/1795402/java-check-a-string-if-there-is-a-special-character-in-it
    */
    public boolean ValidateSpecialChar(String s){

        Pattern p = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher m = p.matcher(s);
        boolean b = m.find();
        return b;

    }

    //Authentication
    public void login(View view) {

        final String PLEASE_VERIFY = "Por favor verifique su usuario o su contraseña";
        final String SPECIAL_CHARACTERS = "Recuerde que no se admiten caracteres especiales";
        String user = username.getText().toString();
        String passwrd = password.getText().toString();

        //Connect with SQLite database
        dbAdapter = new DBAdapter(this);
        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //Get password from USUARIOS table of SQLite
        Cursor getPassword = dbAdapter.getContrasena(user);
        String passwordBD = "";
        if(getPassword.moveToFirst()){
            passwordBD = getPassword.getString(0);
        }
        //Compare entered password versus SQLite database  password
        if (passwordBD.equals(passwrd) && !passwordBD.isEmpty()){
            new WebServiceLoginTask().execute(WEB_SERVICE);

            //Delete user and password from EditTexts
            username.setText("");
            password.setText("");
        } else {

            boolean validPassword = ValidateSpecialChar(passwrd);
            boolean validUser = ValidateSpecialChar(user);
            if (validPassword || validUser)
                Toast.makeText(getApplicationContext(), SPECIAL_CHARACTERS, Toast.LENGTH_LONG).show();
            else
                Toast.makeText(getApplicationContext(), PLEASE_VERIFY, Toast.LENGTH_LONG).show();
        }

        //Get initial Post
        new PostAsyncTask().execute(WEBSERVICE_POSTS);

        dbAdapter.close();

    }


    //Copy Database into app's package
    public void copyDatabaseIntoPackage(){

        @SuppressLint("SdCardPath") String  DEST_DIR = "/data/data/", DB_DIRECTORY ="/databases/";
        String DATABASE_ORIG_NAME = "bitgray", DATABASE_NAME = "Bitgray";

        try {
            String destDir = DEST_DIR + getPackageName() + DB_DIRECTORY;
            String destPath = destDir + DATABASE_NAME;
            File f = new File(destPath);

            if(!f.exists()){
                //---make sure directory exists---
                File directory = new File(destDir);
                if(directory.mkdirs()) {
                    //---copy the db from the assets folder into databases folder---
                    copyDB(getBaseContext().getAssets().open(DATABASE_ORIG_NAME),
                            new FileOutputStream(destPath));
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    //Copy 1 KB from assets to internal Storage
    public void copyDB(InputStream inputStream, OutputStream outputStream) throws IOException {

        byte[] buffer = new byte[BUFFER_SIZE];
        int length;
        while ((length = inputStream.read(buffer))>0){
            outputStream.write(buffer,0,length);
        }
        inputStream.close();
        outputStream.close();
    }


    //*************  INNER CLASSES  *****************

    private class WebServiceLoginTask extends AsyncTask<String,String, String> {



        HttpURLConnection connection = null;
        BufferedReader reader = null;

        @Override
        protected String doInBackground(String... urls) {

            //Taken from : https://www.youtube.com/watch?v=_7r_vdwmW0o

            try {
                URL url = new URL(urls[0]);
                connection = (HttpURLConnection)url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line ;
                while ((line = reader.readLine()) != null){
                    buffer.append(line);
                }

                //Generate Random, taken from :
                // http://stackoverflow.com/questions/363681/generating-random-integers-in-a-specific-range
                Random rand = new Random();
                int x = rand.nextInt(10);
                Log.e("Login", String.valueOf(x));
                id_user = x;

                //Parse JSON

                String finalJson = buffer.toString();

                try {
                    JSONArray parentArray = new JSONArray(finalJson);
                    JSONObject jsonObject =  parentArray.getJSONObject(x);
                    //Retrieve data
                    int id = jsonObject.getInt("id");
                    String strId = String.valueOf(id);
                    String name = jsonObject.getString("name");
                    String username = jsonObject.getString("username");
                    String email = jsonObject.getString("email");
                    String phone = jsonObject.getString("phone");
                    JSONObject companyObject = jsonObject.getJSONObject("company");
                    String companyName = companyObject.getString("name");

                    //Insert into SQLite database
                    try {
                        dbAdapter.open();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                     //Delete this table
                    dbAdapter.deleteFromUsers();

                    //Insert new data
                    if (dbAdapter.insertIntoUsers(strId, name, username, email, phone, companyName) >= 0) {
                        Log.e("Sergio","Successful Insert: "+id);
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection!=null) {
                    connection.disconnect();
                }
                try {
                    if(reader!=null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
            //Launch Splash

            Intent in = new Intent(getApplicationContext(),SplashActivity.class);
            in.putExtra("id",id_user);
            startActivity(in);

            //Determine which initial post to fetch
            switch (id_user){
                case 1:
                    postId = 0;
                    break;
                case 2:
                    postId = 10;
                    break;
                case 3:
                    postId = 20;
                    break;
                case 4:
                    postId = 30;
                    break;
                case 5:
                    postId = 40;
                    break;
                case 6:
                    postId = 50;
                    break;
                case 7:
                    postId = 60;
                    break;
                case 8:
                    postId = 70;
                    break;
                case 9:
                    postId = 80;
                    break;
                case 10:
                    postId = 90;
                    break;
            }

            //Get JSON data for Album
            new WebServiceAlbumTask().execute(WEB_SERVICE_ALBUM);

        }

    }

    private class WebServiceAlbumTask extends AsyncTask<String,String, String>{

        HttpURLConnection connection = null;
        BufferedReader reader = null;
        String title = "";
        int idAlbum = 0;

        @Override
        protected String doInBackground(String... strings) {

            try {
                URL url = new URL(strings[0]);
                connection = (HttpURLConnection)url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line ;
                while ((line = reader.readLine()) != null){
                    buffer.append(line);
                }


                //Parse JSON

                String finalJson = buffer.toString();
                idJsonAlbum = id_user*10;

                try {
                    JSONArray parentArray = new JSONArray(finalJson);
                    JSONObject jsonObject =  parentArray.getJSONObject(idJsonAlbum);
                    //Retrieve data
                    title = jsonObject.getString("title");
                    idAlbum = jsonObject.getInt("id");

                    //Insert into SQLite database
                    try {
                        dbAdapter.open();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    //Delete this table
                    dbAdapter.deleteFromAlbum();

                    //Insert new data
                    if (dbAdapter.insertIntoAlbum(idAlbum, title) >= 0) {
                        Log.e("Sergio","Successful Insert Album: "+idAlbum);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection!=null) {
                    connection.disconnect();
                }
                try {
                    if(reader!=null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Get JSON data for Photos
            new WebServicePhotosTask().execute(WEB_SERVICE_PHOTOS);
        }
    }

    private class WebServicePhotosTask extends AsyncTask<String,String, String>{

        HttpURLConnection connection = null;
        BufferedReader reader = null;
        String urlPhoto = "";
        int idPhoto = 0;

        @Override
        protected String doInBackground(String... strings) {

            try {
                URL url = new URL(strings[0]);
                connection = (HttpURLConnection)url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line ;
                while ((line = reader.readLine()) != null){
                    buffer.append(line);
                }

                //Parse JSON

                String finalJson = buffer.toString();

                try {
                    JSONArray parentArray = new JSONArray(finalJson);

                    try {
                        dbAdapter.open();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }

                    //Delete this table
                    dbAdapter.deleteFromPhotos();

                    //Iterate over fifty photos
                    for (int i = 0; i < 50; i++){

                        int PhotoId = (idJsonAlbum*50)+i;
                        JSONObject jsonObject =  parentArray.getJSONObject(PhotoId);
                        //Retrieve data
                        urlPhoto = jsonObject.getString("url");
                        //Taken from :
                        //http://stackoverflow.com/questions/16702357/how-to-replace-a-substring-of-a-string
                        String newUrlPhoto = urlPhoto.replaceAll("http", "https");
                        idPhoto = jsonObject.getInt("id");
                        //Insert into SQLite database

                        if (dbAdapter.insertIntoPhotos(idPhoto, newUrlPhoto) >= 0) {
                            Log.e("Sergio","Successful Insert idPhoto: "+idPhoto);
                        }

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection!=null) {
                    connection.disconnect();
                }
                try {
                    if(reader!=null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }


    }

    private class PostAsyncTask extends AsyncTask<String,String, String>{


        HttpURLConnection connection = null;
        BufferedReader reader = null;
        String title = "", body = "";
        int idP = 0;


        @Override
        protected String doInBackground(String... urls) {


            try {
                URL url = new URL(urls[0]);
                connection = (HttpURLConnection)url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line ;
                while ((line = reader.readLine()) != null){
                    buffer.append(line);
                }

                dbAdapter.deleteFromPost();

                //Parse JSON

                String finalJson = buffer.toString();

                try {
                    JSONArray parentArray = new JSONArray(finalJson);
                    JSONObject jsonObject =  parentArray.getJSONObject(postId);
                    //Retrieve data
                    idP = jsonObject.getInt("id");
                    title = jsonObject.getString("title");
                    body = jsonObject.getString("body");

                    dbAdapter.insertIntoPost(idP, title, body);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection!=null) {
                    connection.disconnect();
                }
                try {
                    if(reader!=null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);
            //Get comments for this post
            new CommentAsyncTask().execute(WEBSERVICE_COMMENTS);

        }
    }


    private class CommentAsyncTask extends AsyncTask<String,String, String>{

        HttpURLConnection connection = null;
        BufferedReader reader = null;
        int idCommentForDB = 0;
        String name = "", bodyComment = "", email = "";



        @Override
        protected String doInBackground(String... strings) {

            try {
                URL url = new URL(strings[0]);
                connection = (HttpURLConnection)url.openConnection();
                connection.connect();

                InputStream stream = connection.getInputStream();
                reader = new BufferedReader(new InputStreamReader(stream));

                StringBuffer buffer = new StringBuffer();
                String line ;
                while ((line = reader.readLine()) != null){
                    buffer.append(line);
                }


                dbAdapter.deleteFromComments();
                //Parse JSON

                String finalJson = buffer.toString();

                try {
                    JSONArray parentArray = new JSONArray(finalJson);
                    //Iterate over five comments
                    for (int i = 0; i < 5; i++){
                        int idComment = (postId*5)+i;
                        JSONObject jsonObject =  parentArray.getJSONObject(idComment);
                        //Retrieve data
                        idCommentForDB = jsonObject.getInt("id");
                        name = jsonObject.getString("name");
                        email = jsonObject.getString("email");
                        bodyComment = jsonObject.getString("body");

                        dbAdapter.insertIntoComments(idCommentForDB, name, email, bodyComment);

                    }



                } catch (JSONException e) {
                    e.printStackTrace();
                }

                return buffer.toString();


            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (connection!=null) {
                    connection.disconnect();
                }
                try {
                    if(reader!=null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {

            super.onPostExecute(s);

        }
    }



}
