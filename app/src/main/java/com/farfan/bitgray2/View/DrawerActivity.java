package com.farfan.bitgray2.View;

import android.content.res.Configuration;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;

import com.farfan.bitgray2.R;

public class DrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    //**************  GLOBAL ATTRIBUTES  *********************
    NavigationView navigationView = null;
    Toolbar toolbar = null;
    int  value = 0;

    //**************  OVERRIDE METHODS  *********************
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        // Receive user id from SplashActivity

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            value = extras.getInt("id");
        }
        Log.e("Drawer", String.valueOf(value));



        //Set the fragment initially
        User frgUSer = new User();
        FragmentTransaction frgTransaction = getSupportFragmentManager().beginTransaction();
        frgTransaction.replace(R.id.fragment_container, frgUSer);
        frgTransaction.commit();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    @Override
    public void onBackPressed() {

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_user) {

            User frgUser = new User();
            FragmentTransaction  frgTransaction = getSupportFragmentManager().beginTransaction();
            frgTransaction.replace(R.id.fragment_container, frgUser);
            frgTransaction.commit();

         } else if (id == R.id.nav_gallery) {

            Gallery frgGallery = new Gallery();
            FragmentTransaction frgTransaction = getSupportFragmentManager().beginTransaction();
            frgTransaction.replace(R.id.fragment_container, frgGallery);
            frgTransaction.commit();

        } else if (id == R.id.nav_post) {

            Post frgPost = new Post();
            FragmentTransaction frgTransaction = getSupportFragmentManager().beginTransaction();
            frgTransaction.replace(R.id.fragment_container, frgPost);
            frgTransaction.commit();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }


    //**************  PROPER METHODS  *********************

     public int MyMethod(){

        return value;
    }
}
