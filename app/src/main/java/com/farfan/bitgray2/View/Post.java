package com.farfan.bitgray2.View;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.farfan.bitgray2.Controller.ListCommentAdapter;
import com.farfan.bitgray2.Model.DBAdapter;
import com.farfan.bitgray2.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class Post extends Fragment {

    int userId = 0;
    int postId = 0;
    TextView tvTitle, tvBody;
    ListView lvComments;
    ListCommentAdapter listCommentAdapter;
    //ArrayList to populate ListCommentAdapter
    ArrayList<String> arrListName = new ArrayList<>(), arrListBodyComment = new ArrayList<>(),
            arrListEmail = new ArrayList<>();

    public Post() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        //Get user id from DRawerActivity
        DrawerActivity drawerActivity = (DrawerActivity)getActivity();
        userId  = drawerActivity.MyMethod();
        Log.e("Post", String.valueOf(userId));

        // Inflate the layout for this fragment

        View rootView = inflater.inflate(R.layout.fragment_post, container, false);

        tvTitle = (TextView)rootView.findViewById(R.id.tvTitle);
        tvBody = (TextView)rootView.findViewById(R.id.tvBody);
        lvComments = (ListView) rootView.findViewById(R.id.lvComments);

        DBAdapter dbAdapter =  new DBAdapter(getActivity());
        try {
            dbAdapter.open();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        //GEt initial post from SQLIte
        Cursor cStpValues = dbAdapter.getPost();

        if(cStpValues.moveToFirst()){
            do{
                String strName = cStpValues.getString(1);
                String strBody = cStpValues.getString(2);

                //Show all values

                tvTitle.setText(strName);
                tvBody.setText(strBody);

            }while (cStpValues.moveToNext());

        }

        //GEt comments from SQLite
        Cursor curPhotos = dbAdapter.getComments();
        if(curPhotos.moveToFirst()){
            do{
                String name = curPhotos.getString(1);
                String email = curPhotos.getString(2);
                String bodyComment = curPhotos.getString(3);

                //Arrange comments for Listview

                arrListName.add(name);
                arrListEmail.add(email);
                arrListBodyComment.add(bodyComment);

            }while (curPhotos.moveToNext());

        }

        dbAdapter.close();


        //Send ArrayList to ListCommentAdapter
        listCommentAdapter = new ListCommentAdapter(getActivity(),  arrListEmail, arrListBodyComment, arrListName);
        lvComments.setAdapter(listCommentAdapter);

        return rootView;
    }


    //*******************INNER CLASSES******************






}
