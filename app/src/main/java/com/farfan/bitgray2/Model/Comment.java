package com.farfan.bitgray2.Model;

/**
 * Created by developer on 2/02/16.
 * Model class for comments
 */
public class Comment {


    //*********  Global attributes  ***********
    private String commentTitle, email, comment;


    //Constructor
    public Comment(String commentTitle, String email,  String comment){

        this.comment = comment;
        this.commentTitle = commentTitle;
        this.email = email;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCommentTitle() {
        return commentTitle;
    }

    public void setCommentTitle(String commentTitle) {
        this.commentTitle = commentTitle;
    }


}
