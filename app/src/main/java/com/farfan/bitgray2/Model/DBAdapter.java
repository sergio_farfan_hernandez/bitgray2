package com.farfan.bitgray2.Model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.sql.SQLException;

/**
 * Created by developer on 31/01/16.
 */
public class DBAdapter {

    //********** GLOBAL ATTRIBUTES ************
    static final String TAG = "DBAdapter";
    static final String DATABASE_NAME = "Bitgray";
    static final int DATABASE_VERSION = 1;
    final Context context;
    DatabaseHelper DBHelper;
    SQLiteDatabase db;

    //*****CONSTRUCTOR*******
    public DBAdapter(Context ctx){
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    //*********INNER CLASSES********
    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            Log.w(TAG, "Upgrading database from version " + oldVersion + "to "
                    + newVersion + ", which will destroy all old data");
            onCreate(db);
        }
    }

    //**********PROPER METHODS**********

    //Open the DB
    public DBAdapter open() throws SQLException {
        db = DBHelper.getWritableDatabase();
        return this;
    }
    //Close the DB
    public void close()
    {
        DBHelper.close();
        //db.close();
    }

    //-----------------------   Selects    ---------------------------
    public Cursor getContrasena(String user){
        return db.rawQuery("SELECT contrasena FROM USUARIOS WHERE usuario = '"+ user +"'",null);
    }

    public Cursor getUSer(){
        return db.rawQuery("SELECT * FROM USERS ",null);
    }

    public Cursor getAlbum(){
        return db.rawQuery("SELECT * FROM ALBUM ",null);
    }

    public Cursor getPhotos(){
        return db.rawQuery("SELECT * FROM PHOTOS ",null);
    }

    public Cursor getComments(){
        return db.rawQuery("SELECT * FROM COMMENTS ",null);
    }

    public Cursor getPost(){
        return db.rawQuery("SELECT * FROM POST ",null);
    }
    //-----------------------   Inserts    ---------------------------


    public long insertIntoUsers (String id, String name, String username, String  email, String phone, String  companyName){
        ContentValues initialValues = new ContentValues();
        initialValues.put("id", id);
        initialValues.put("name", name);
        initialValues.put("username", username);
        initialValues.put("email", email);
        initialValues.put("phone", phone);
        initialValues.put("companyName", companyName);

        return db.insert("USERS", null, initialValues);
    }

    public long insertIntoAlbum (int id, String title){
        ContentValues initialValues = new ContentValues();
        initialValues.put("_id", id);
        initialValues.put("title", title);

        return db.insert("ALBUM", null, initialValues);
    }

    public long insertIntoPost (int id, String title, String body){
        ContentValues initialValues = new ContentValues();
        initialValues.put("_id", id);
        initialValues.put("title", title);
        initialValues.put("body", body);

        return db.insert("POST",null ,initialValues);
    }

    public long insertIntoComments (int id, String name, String email, String body){
        ContentValues initialValues = new ContentValues();
        initialValues.put("_id", id);
        initialValues.put("name", name);
        initialValues.put("email", email);
        initialValues.put("body", body);

        return db.insert("COMMENTS",null ,initialValues);
    }

    public long insertIntoPhotos (int id, String url){
        ContentValues initialValues = new ContentValues();
        initialValues.put("_id", id);
        initialValues.put("url", url);

        return db.insert("PHOTOS",null ,initialValues);
    }
    //-----------------------   Deletes    ---------------------------



    public void deleteFromUsers() {
        db.execSQL("DELETE FROM USERS");
    }

    public void deleteFromAlbum() {
        db.execSQL("DELETE FROM ALBUM");
    }

    public void deleteFromPost() {
        db.execSQL("DELETE FROM POST");
    }

    public void deleteFromComments() {
        db.execSQL("DELETE FROM COMMENTS");
    }

    public void deleteFromPhotos() {
        db.execSQL("DELETE FROM PHOTOS");
    }

}
