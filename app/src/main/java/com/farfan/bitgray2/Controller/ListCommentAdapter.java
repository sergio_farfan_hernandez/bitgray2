package com.farfan.bitgray2.Controller;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.farfan.bitgray2.R;
import java.util.ArrayList;

/**
 * Created by developer on 2/02/16.
 */
public class ListCommentAdapter  extends BaseAdapter {

    //**********GLOBAL ATTRIBUTES**********

    Context context;
    ArrayList<String> arrayListEmail, arrayListBodyComment, arrayListName;
    LayoutInflater inflater;

    //CONSTRUCTOR
    public ListCommentAdapter(Context context, ArrayList<String> arrayListEmail,
                              ArrayList<String> arrayListBodyComment, ArrayList<String> arrayListName){
        this.context = context;
        this.arrayListEmail = arrayListEmail;
        this.arrayListBodyComment = arrayListBodyComment;
        this.arrayListName = arrayListName;

    }


    //*********OVERRIDE METHODS***************
    @Override
    public int getCount() {
        return arrayListName.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View itemView = inflater.inflate(R.layout.lv_row_comment, viewGroup, false);

        TextView texName   = (TextView) itemView.findViewById(R.id.tvTitleComment);
        TextView  textEmail = (TextView) itemView.findViewById(R.id.tvEmail);
        TextView  textBodyComment   = (TextView) itemView.findViewById(R.id.tvBodyComment);
        //**************Set corresponding values****************
        texName.setText(arrayListName.get(i));
        textEmail.setText(arrayListEmail.get(i));
        textBodyComment.setText(String.valueOf(arrayListBodyComment.get(i)));

        return itemView;
    }
}
